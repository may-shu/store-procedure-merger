package com.paras.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

/**
 * Merger to merge procedures in a directory into a single file.
 * @author Paras
 *
 */
public class Merger {
	
	private static Logger LOGGER = Logger.getLogger( Merger.class );

	public static void main( String ... args ) {
		LOGGER.info( "Merger | Starting");
		
		String workingDirectory, dirToScan, out;
		boolean allParamsPresent = true;
		
		workingDirectory = dirToScan = out = null;
		
		Options options = new Options();
		
		options.addOption( "w", true, "Working Directory" );
		options.addOption( "d", true, "Directory to scan" );
		options.addOption( "o", true, "Output File Name" );
		
		CommandLineParser parser = new DefaultParser();
		
		try{
			CommandLine command = parser.parse( options, args );
			
			if( command.hasOption( "w" )) {
				workingDirectory = command.getOptionValue("w" );
				LOGGER.info( "Merger | Will use " + workingDirectory + " as working directory.");
			} else {
				allParamsPresent = false;
				LOGGER.error( "Merger | No working directory provided.");
			}
			
			if( command.hasOption( "d" )) {
				dirToScan = command.getOptionValue( "d" );
				LOGGER.info( "Merger | Will use " + dirToScan + " as scanning directory." );
			} else {
				allParamsPresent = false;
				LOGGER.error( "Merger | No Scanning Directory Provided.");
			}
			
			if( command.hasOption( "o" )) {
				out = command.getOptionValue( "o" );
				LOGGER.info( "Merger | Will use " + out + " as output file.");
			} else {
				allParamsPresent = false;
				LOGGER.error( "Merger | No output file provided.");
			}
			
			if( allParamsPresent ) {
				
				String pathToScan = workingDirectory + File.separator + dirToScan;
				String outFile = workingDirectory + File.separator + out;
				
				File dump = new File( outFile );
				File scanDir = new File( pathToScan );
				
				BufferedWriter writer = new BufferedWriter( new FileWriter( dump ));
				
				File[] children = scanDir.listFiles();
				int count = 1;
				
				for( File child : children ) {
					if( child.isFile() ) {
						String name = child.getName();
						
						if( name.endsWith( ".sql")) {
							LOGGER.info( count++ + " Handling " + name );
							
							writer.write( "DELIMITER ;;");
							writer.newLine();
							
							BufferedReader reader = new BufferedReader( new FileReader( child ));
							String currLine = null;
							
							while( (currLine = reader.readLine()) != null ) {
								writer.write( currLine );
								writer.newLine();
							}
							
							reader.close();
							
							writer.write( "DELIMITER ;");
							writer.newLine();
						}
					}
				}
				
				writer.flush();
				
				writer.close();
				
			}
		}
		catch (ParseException ex) {
				LOGGER.error( "Runner | Merger ParseException | " + ex.getMessage());
		}
		catch( IOException ex ) {
			LOGGER.error( "Runner | Caught IOException | " + ex.getMessage());
		}
		
		LOGGER.info( "Merger | All Operations Done.");
	}
	
}
